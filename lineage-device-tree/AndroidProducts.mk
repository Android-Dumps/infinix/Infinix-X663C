#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X663C.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X663C-user \
    lineage_Infinix-X663C-userdebug \
    lineage_Infinix-X663C-eng
